<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('employees', 'EmployeesController@index');
Route::get('employee/{id}/{page}', 'EmployeesController@show');
Route::post('employee', 'EmployeesController@store');
Route::put('employee', 'EmployeesController@edit');
Route::delete('employee/{id}', 'EmployeesController@destroy');


Route::get('companies', 'CompaniesController@index');
Route::get('company/{id}', 'CompaniesController@show');
Route::post('company', 'CompaniesController@store');
Route::put('company', 'CompaniesController@edit');
Route::delete('company/{id}', 'CompaniesController@destroy');
