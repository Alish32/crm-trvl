<?php

use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'CompanyController@index')->name('company');
Route::get('/employees', 'EmployeeController@index')->name('employee');
Route::get('/companies', 'CompanyController@index')->name('company');

//Route::get('/lang/{locale}', 'LocalizationController@index');
