Our program is briefly a small CRM system that allows us to add, delete, and edit the company and its employees.

We need to follow the steps below to run the project:

If you are a windows user

-	Download wamp: http://www.wampserver.com/en/
-	Download and extract cmder mini:
	https://github.com/cmderdev/cmder/releases/download/v1.1.4.1/cmder_mini.zip
	Update windows environment variable path to point to your php install folder (inside wamp installation dir) 
	Here is how you can do this:
	http://stackoverflow.com/questions/17727436/how-to-properly-set-php-environment-variable-to-run-commands-in-git-bash

If you are a Mac OS, Ubuntu, or Windows users continue here:

-	Create a database locally named crm
-	Download composer https://getcomposer.org/download/
-	Pull the project https://bitbucket.org/Alish32/crm-trvl/
-	Open the console and cd your project root directory
-	Run composer install or php composer.phar install
-	Run php artisan migrate
-	Run php artisan db:seed to run seeders
-	Run php artisan serve
-	Run php artisan key:generate
-	Run npm install to install a package and any packages that it depends on.
-	Run npm run watch to combine all your Vue components and other JavaScript files into a browser-friendly combined file. It will "watches" for updates to your .vue and .js files. If it detects a change, it'll re-build the browser-friendly file so you can just refresh the page.

You can now access the project at localhost:8000
