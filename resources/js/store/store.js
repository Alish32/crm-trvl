import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
    state: {
        drawerState: false
    },
    mutations: {
        setDrawerState (state, mystate) {
            state.drawerState = mystate
        }
    },
    actions: {
        updateDrawerState({ commit }, state) {
            commit('setDrawerState', state)
        },
    }
})

export default store;
