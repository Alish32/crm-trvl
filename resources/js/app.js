/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/Container.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('nav-bar', require('./components/NavBar.vue').default);
Vue.component('drawer', require('./components/Drawer.vue').default);
Vue.component('employees', require('./Employees.vue').default);
Vue.component('companies', require('./Companies.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
import Vue from 'vue'
import Vuex from 'vuex'
import store from './store/store';
import Vuetify from 'vuetify';

Vue.use(Vuetify);
Vue.use(Vuex)
const app = new Vue({
    el: '#app',
    store: store,
    vuetify: new Vuetify(),
});
