<?php

namespace App\Http\Controllers;

use App\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class   CompaniesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = Company::orderBy('created_at', 'desc')->get()->toArray();
        return $companies;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //return View::make('nerds.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(), [
            'name' => 'required']);
        $company = $request->isMethod('put') ? Company::findOrFail($request->id) : new Company;
        $company->name = $request->input('name');
        $company->email = $request->input('email');
        $company->logo = $request->input('logo');
        $company->website = $request->input('website');

        $this->sendEmail($company);

        if ($company->save()) {
            return $company;
        }
    }

    public function sendEmail($request){
        $data=[
            'mail_address'=> 'necefzade.elish@gmail.com',
            'name'=> 'Alish'
        ];
        Mail::send('emails.app_test',$data,function($mail) use ($data) {
            $mail->subject('Company Created');
            $mail->from('necefzade.elish@mail.ru','Alish');
            $mail->to($data['mail_address']);
        });
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $company = Company::findOrFail($id);
        return $company;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $company = Company::findOrFail($request->id);
        $company->name = $request->input('name');
        $company->email = $request->input('email');
        $company->logo = $request->input('logo');
        $company->website = $request->input('website');

        $this->sendEmail($request);

        if ($company->save()) {
            return $company;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $company = Company::findOrFail($id);
        if ($company->delete()) {
            return $company;
        }
    }
}
